//Alessandro Vendramin - 1933413

package utilities;

public class MatrixMethod {
	
	public static int[][] duplicate(int[][] array){
		int column = array.length;
		int row = array[0].length;
		
		int[][] temp = new int[column*2][row];
		int count = 0;
		for(int i = 0; i < column; i++) {
			count = i * 2;
			temp[count] = array[i];
			temp[count+1] = array[i];
		}
		
		return temp;
	}
}
