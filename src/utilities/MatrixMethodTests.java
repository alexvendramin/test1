//Alessandro Vendramin - 1933413

package utilities;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MatrixMethodTests {

	@Test
	void testDuplicate() {
		int[][] array = new int[2][3];
		array[0][0] = 1;
		array[1][0] = 2;
		array[0][1] = 3;
		array[1][1] = 4;
		array[0][2] = 5;
		array[1][2] = 6;
		int[][] array2 = MatrixMethod.duplicate(array);
		
		int[][] array3 = new int[4][3];
		array3[0][0] = 1;
		array3[1][0] = 1;
		array3[2][0] = 2;
		array3[3][0] = 2;
		array3[0][1] = 3;
		array3[1][1] = 3;
		array3[2][1] = 4;
		array3[3][1] = 4;
		array3[0][2] = 5;
		array3[1][2] = 5;
		array3[2][2] = 6;
		array3[3][2] = 6;
		System.out.println(array2[2][1]);
		assertEquals(array2[0][0], array3[0][0]);
		assertEquals(array2[1][0], array3[1][0]);
		assertEquals(array2[2][0], array3[2][0]);
		assertEquals(array2[3][0], array3[3][0]);
		assertEquals(array2[0][1], array3[0][1]);
		assertEquals(array2[1][1], array3[1][1]);
		assertEquals(array2[2][1], array3[2][1]);
		assertEquals(array2[3][1], array3[3][1]);
		assertEquals(array2[0][2], array3[0][2]);
		assertEquals(array2[1][2], array3[1][2]);
		assertEquals(array2[2][2], array3[2][2]);
		assertEquals(array2[3][2], array3[3][2]);
	}

}
