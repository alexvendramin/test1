//Alessandro Vendramin - 1933413

package tests;

public class Cars {
		private int speed;
		private int location;
		static int MAX_POSITION = 30;
		
		public Cars(int speed){
			if(speed < 0) {
				System.out.println("Negative speeds are not allowed.");
			}
			else {
				this.speed = speed;
				this.location = MAX_POSITION;
				this.location = this.location / 2;
			}
		}
		
		public int getSpeed() {
			return this.speed;
		}
		
		public int getLocation() {
			return this.location;
		}
		
		public void moveRight() {
			if(this.location + this.speed > MAX_POSITION) {
				this.location = MAX_POSITION;
			}
			else {
				this.location = this.location + this.speed;
			}
		}
		
		public void moveLeft() {
			if(this.location - this.speed < 0) {
				this.location = 0;
			}
			else {
				this.location = this.location - this.speed;
			}
		}
		
		public void accelerate() {
			this.speed++;
		}
		
		public void stop() {
			this.speed = 0;
		}
		
	}
