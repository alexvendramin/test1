//Alessandro Vendramin - 1933413

package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CarTests {

	@Test
	void testCars() {
		Cars car = new Cars(-2);
		assertEquals(car.getSpeed(), 0);
		assertEquals(car.getLocation(), 0);
		Cars car2 = new Cars(25);
		assertEquals(car2.getSpeed(), 25);
		assertEquals(car2.getLocation(), 15);
		Cars car3 = new Cars(0);
		assertEquals(car3.getSpeed(), 0);
		assertEquals(car3.getLocation(), 15);
	}
	
	@Test
	void testGetSpeed() {
		Cars car = new Cars(10);
		assertEquals(car.getSpeed(), 10);		
	}
	
	@Test
	void testGetLoation() {
		Cars car = new Cars(10);
		assertEquals(car.getLocation(), 15);
	}
	
	@Test
	void testMoveRight() {
		Cars car = new Cars(10);
		assertEquals(car.getLocation(), 15);
		car.moveRight();
		assertEquals(car.getLocation(), 25);
		Cars car2 = new Cars(50);
		car2.moveRight();
		assertEquals(car2.getLocation(), 30);
	}
	
	@Test
	void testMoveLeft() {
		Cars car = new Cars(10);
		assertEquals(car.getLocation(), 15);
		car.moveLeft();
		assertEquals(car.getLocation(), 5);
		Cars car2 = new Cars(50);
		car2.moveLeft();
		assertEquals(car2.getLocation(), 0);
	}
	
	@Test
	void testAccelerate() {
		Cars car = new Cars(10);
		car.accelerate();
		assertEquals(car.getSpeed(), 11);
	}
	
	@Test
	void testStop() {
		Cars car = new Cars(10);
		car.stop();
		assertEquals(car.getSpeed(), 0);
	}
}
